<?php
/**
 * FlexiBee Digest 
 *
 * @author     Vítězslav Dvořák <info@vitexsofware.cz>
 * @copyright  (G) 2017 Vitex Software
 */

namespace FlexiPeeHP\Digest;

/**
 *
 * @author vitex
 */
interface DigestModuleInterface
{

    public function heading();

    public function dig();
}
